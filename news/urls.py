# -*- coding: utf-8 -*-

from django.conf.urls.defaults import *  

from views import NewsDetailView

urlpatterns = patterns('news.views',
    url(r'view/news/(?P<news_id>\d+)$', NewsDetailView.as_view(), name='news_news_view'),
    url(r'view/news/add$', NewsDetailView.as_view(), name='news_news_view_add'),
    # url(r'list/news/$', 'list_news', name='news_news_list'),
    url(r'calendar$', 'calendar_dates', name='calendar_dates'),
    url(r'category/(?P<cat_id>\d+)/toggle-publ$','category_toggle_publ'),
    url(r'list/news/(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})$', 'list_news', name='news_news_list_date'),
    url(r'list/news/cat/(?P<cat_id>\d*)$', 'list_news', name='news_news_list_category'),
)