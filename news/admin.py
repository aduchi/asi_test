# -*- coding: utf-8 -*-

from django.contrib import admin
from models import *


class NewsAdmin(admin.ModelAdmin):
    """
    News admin class
    """
    # search_fields = ('name', 'id')
    # list_filter = ('creation_date','modification_date') 
    # list_display = ('id', 'name')
    pass

admin.site.register(News, NewsAdmin)

class CategoryAdmin(admin.ModelAdmin):
    """
    Category admin class
    """
    # search_fields = ('name', 'id')
    # list_filter = ('creation_date','modification_date') 
    # list_display = ('id', 'name')
    pass

admin.site.register(Category, CategoryAdmin)

