# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models


class PublishedManager(models.Manager):
    def get_query_set(self):
        return super(PublishedManager, self).get_query_set().filter(published=True)

    def dates(self,year,month):
        news_dates = self.get_query_set().filter(add_date__year=year,add_date__month=month).dates('add_date','day').distinct()
        return [date.day for date in news_dates]

class News(models.Model):
    
    category = models.ForeignKey('news.Category',null=True,blank=True, verbose_name="Рубрика")
    title = models.CharField(verbose_name="Заголовок", max_length=255)
    body = models.TextField(verbose_name="Новость")
    published = models.BooleanField(verbose_name="Опубликовано")
    add_date = models.DateTimeField(verbose_name="Дата добавления", auto_now_add=True)
    author = models.ForeignKey(User,null=True,blank=True,verbose_name="Автор")
    
    objects = models.Manager() # The default manager.
    pub_objects = PublishedManager()

    class Meta:
      verbose_name = u'Новость'
      verbose_name_plural = u'Новости'
      ordering = ['-add_date']


    @models.permalink
    def get_absolute_url(self):
        return ('news_news_view', [str(self.id)])

    def __unicode__(self):
        return self.title

    def get_teaser(self):
	blocks = self.body.split('\n')
        if len(blocks) < 2:
	    blocks = self.body.split(' ')
	    return ' '.join(blocks[:30])
	else:
            return blocks[0]

    def get_body(self):
        blocks = self.body.split('\n')
        if len(blocks) < 2:
            blocks = self.body.split(' ')
            return ' '.join(blocks[30:])
        else:
            return ''.join(blocks[2:])

    def get_related_news(self):
        return News.pub_objects.filter(category=self.category).exclude(id=self.id)[:5]


class Category(models.Model):
    
    
    title = models.CharField(verbose_name="Заголовок", max_length=255)
    published = models.BooleanField(verbose_name="Опубликовано")
    
    objects = models.Manager() # The default manager.
    pub_objects = PublishedManager()


    class Meta:
      verbose_name = u'Рубрика'
      verbose_name_plural = u'Рубрики'
      ordering = ['title']


    def __unicode__(self):
        return self.title


