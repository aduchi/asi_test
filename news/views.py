# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.shortcuts import get_object_or_404
from django.utils import simplejson as json
from django.http import HttpResponse,HttpResponseForbidden
from django.views.generic.edit import UpdateView

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required
from django.core import serializers

import datetime

from models import *
from forms import *

class DetailView(UpdateView):
    context_object_name = 'object'
    
    def get_object(self,queryset=None):
        try:
            obj = super(DetailView,self).get_object(queryset)
        except AttributeError:
            obj = self.model()
        return obj

    def get_form(self,form_class):
	   kwargs=self.get_form_kwargs()
	   kwargs['user']=self.request.user
	   return form_class(**kwargs)

class NewsDetailView(DetailView):
    template_name = 'news_view.html'
    model = News
    pk_url_kwarg = 'news_id'
    form_class = NewsForm

    @method_decorator(permission_required('news.add_news'))
    def post(self, request,*args,**kwargs):
	   return super(NewsDetailView,self).post(request, *args, **kwargs)


def view_news(request, news_id):
    """
    Allow input of data on news
    """
    news = get_object_or_404(News, pk=news_id)
    context = {'object': news }
    return render_to_response('news_view.html',
                              context,
                              context_instance=RequestContext(request))

@permission_required('news.add_news')
def category_toggle_publ(request,cat_id):
    category = get_object_or_404(Category,pk=cat_id)
    category.published = not category.published
    category.save()
    return HttpResponse(serializers.serialize('json', [category,]),
                                 content_type='application/json')    

def calendar_dates(request):
    year  = request.GET.get('year', datetime.datetime.now().strftime("%Y"))
    month = request.GET.get('month', datetime.datetime.now().strftime("%-m"))
    return HttpResponse(json.dumps(News.pub_objects.dates(year,month)),
                                 content_type='application/json')


def list_news(request,*args,**kwargs):
    """
    Display a list of News paginated
    """
	
    if request.user.is_staff:
        news_list = News.objects.all()
        category_list = Category.objects.all()
    else:
        news_list = News.pub_objects.all()
        category_list = Category.pub_objects.all()

    if ('year' in kwargs):
        news_list = news_list.filter(add_date__year=kwargs['year'])

    if ('month' in kwargs):
        news_list = news_list.filter(add_date__month=kwargs['month'])

    if ('day' in kwargs):
        news_list = news_list.filter(add_date__day=kwargs['day'])

    if ('cat_id' in kwargs):
        news_list = news_list.filter(category__id=kwargs['cat_id'])

    month = datetime.datetime.now().strftime("%-m")
    year = datetime.datetime.now().strftime("%Y")

    paginator = Paginator(news_list, 15)

    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    # If page request (9999) is out of range, deliver last page of results.
    try:
        news_list = paginator.page(page)
    except (EmptyPage, InvalidPage):
        news_list = paginator.page(paginator.num_pages)

    context = {'news_list': news_list , 'category_list': category_list, 'dates': News.pub_objects.dates(year,month)}
    return render_to_response('news_list.html',
                              context,
                              context_instance=RequestContext(request))







