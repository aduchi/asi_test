# -*- coding: utf-8 -*-

from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field, Div, Button
from crispy_forms.bootstrap import FormActions, FieldWithButtons, StrictButton

from models import *


class NewsForm(forms.ModelForm):

    category_new = forms.CharField(label=u'Новая рубрика',required=False)

    class Meta:
        model = News
        exclude = ('author',)

    helper = FormHelper()
    helper.layout = Layout(
        Div(
            Field('title',css_class="input-block-level"),
#	    FieldWithButtons(
#		'category_new',
#		StrictButton('<i class="icon-remove"></i>'),
#		StrictButton('<i class="caret"></i>'),
#	    ),
	    Div(
	        Div(Field('category',css_class="input-xxlarge"),css_class="pull-left"),
            StrictButton(
                '<i class="icon-plus"></i>',
                **{
                    'style':"margin-top: 25px;",
                    'class':"pull-right",
                    'data-toggle':"collapse",
                    'data-target':'#category_new'
                }
            ),
		css_class="clearfix",
	    ),
	    Div(Field('category_new',css_class="input-block-level"),css_class="collapse", id = 'category_new'),
            Field('body',css_class="input-block-level",rows=20),
            'published',
            css_class="control-group",
        ),
        FormActions(
            Submit('save', u'Сохранить', css_class="btn-primary pull-right"),
        )
    )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        return super(NewsForm, self).__init__(*args, **kwargs)

    def save(self, *args, **kwargs):
        kwargs['commit']=False
        obj = super(NewsForm, self).save(*args, **kwargs)
        if ((not obj.author) and self.user):
            obj.author=self.user
        obj.save()
        return obj

    def clean_category(self):
        data = self.cleaned_data['category']
        if (not data):
            title = self. _raw_value('category_new')
            data = Category.get_or_create(title=title)
            data.save()
        return data


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category



