from django.conf.urls.defaults import *
from django.conf import settings
from django.contrib.auth.views import logout
import os

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'news.views.list_news', name='news_news_list'),
    (r'^news/', include('news.urls')),
#    (r'^logout$', logout , {'next_page':'/',}, name="logout"),
    url(r'^logout$',logout,{'next_page':'/',},name="logout"),
    (r'^',include('vk_api.urls')),    

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'^admin/', include(admin.site.urls)),

    

)

urlpatterns += patterns('django.views.generic.simple',
    url(r'^$', 'direct_to_template', {'template': 'homepage.html'}, name="homepage"),
)

if settings.DEBUG:
    urlpatterns+= patterns('',
        (r'media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )

