
from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class VK_user(models.Model):
	vk_id = models.IntegerField()
	access_token = models.CharField(max_length=200)
	expire_token = models.IntegerField()
	photo_50 = models.CharField(max_length=500)
	user = models.OneToOneField(User)
