# -*- coding: utf-8 -*-

from django.conf.urls.defaults import *

urlpatterns = patterns('vk_api.views',
    url(r'vk_callback$', 'vk_callback', name='vk_callback'),
)

