# Create your views here.
from django.utils.encoding import iri_to_uri
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from pytils.translit import slugify
from models import *
import requests

def vk_callback(request):
	if ('code' in request.GET):
		payload = {
			'client_id':'3216672',
			'client_secret':'VvR8qHfsNdf3SRQgCW2Z',
			'code':request.GET.get('code'),
			'redirect_uri':iri_to_uri('http://109.227.252.153:8000/vk_callback'),
		}

		r = requests.get('https://oauth.vk.com/access_token',params=payload)
		try:
			user = VK_user.objects.get(vk_id=r.json.get('user_id')).user
		except:
			payload = {'uids':r.json.get('user_id'),'access_token':r.json.get('access_token'),'fields':'photo_50'}
			profile = requests.get('https://api.vk.com/method/users.get',params=payload)
			profile = profile.json.get('response')[0]
			user = User.objects.create_user(username=slugify(profile.get('last_name')+' '+profile.get('first_name')))
			user.first_name=profile.get('first_name')
			user.last_name=profile.get('last_name')
			user.save()
			VK_user(vk_id=r.json.get('user_id'),access_token=r.json.get('access_token'),expire_token=r.json.get('expires_in'),user=user,photo_50=profile.get('photo_50')).save()
		user = authenticate(user=user)
		login(request, user)

	return HttpResponseRedirect('/')
