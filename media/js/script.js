$(function(){
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');
    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type)) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    $('.cat-publ-link').click( function(e){
        e.preventDefault();
        context = {
            url: "/news/category/"+$(this).data("cat-id")+"/toggle-publ",
            type: "POST",
            btn: this,
            success: function(data, textStatus, jqXHR){
                if (data[0].fields.published){
                    $(this.btn).parent().removeClass('muted');
                    $(this.btn).children('i').removeClass('icon-eye-open');
                    $(this.btn).children('i').addClass('icon-eye-close');
                } else {
                    $(this.btn).parent().addClass('muted');
                    $(this.btn).children('i').removeClass('icon-eye-close');
                    $(this.btn).children('i').addClass('icon-eye-open');
                }
            }
        }
        $.ajax(context);
    });
});